#include <lib/common/circular_buffer.h>
#include <lib/driver/uart.h>

DEFINE_CIRCULAR_BUFFER(cbuf, 9);

bool uart_init_called = false;
static retval_t mock_uart_init(uart_t *uart) {
    uart_init_called = true;
    return RV_SUCCESS;
}

uart_t uart_mock = {
    .config = DECLARE_UART_CONFIG(0, 9600, 8, 1, 0, 0, 1),
    .cbuf = &cbuf,
    .send = NULL,  // not going to send
    .recv = NULL,  // or receive anything
    .init = mock_uart_init,
};


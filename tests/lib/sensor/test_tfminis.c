#include <lib/driver/uart.h>
#include <lib/sensor/tfminis.h>

#include "unity_fixture.h"

extern bool uart_init_called;
extern circular_buffer_t cbuf;
extern uart_t uart_mock;

static DEFINE_TFMINIS(tfmini, &uart_mock);

TEST_GROUP(tfminis);

TEST_SETUP(tfminis) {
    cbuf.write_pos = 0;
    cbuf.read_pos = 0;
    cbuf.full = false;
    uart_init_called = false;
}
TEST_TEAR_DOWN(tfminis) {}

TEST(tfminis, test_regular_operation) {
    retval_t rv;
    TEST_ASSERT_FALSE(uart_init_called);
    rv = tfmini.init(&tfmini);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_TRUE(uart_init_called);

    // this happens in background, the isr will populate the cbuf
    frame_t frame = DECLARE_FRAME_SPACE(9);
    uint8_t tfmini_frame[9] = {0x59, 0x59, 0x33, 0x0A, 0x12, 0x3A, 0xAC, 0x09, 0xF0};
    frame.buf = tfmini_frame;
    rv = circular_buffer_write_frame(&cbuf, &frame);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);

    rv = tfmini.update(&tfmini);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);
    TEST_ASSERT_EQUAL(2611, tfmini.data.dist_cm);
    TEST_ASSERT_EQUAL(14866, tfmini.data.strenght);
    TEST_ASSERT_EQUAL_FLOAT(53.5, tfmini.data.temp_c);
}

TEST_GROUP_RUNNER(tfminis) {
    RUN_TEST_CASE(tfminis, test_regular_operation);
}

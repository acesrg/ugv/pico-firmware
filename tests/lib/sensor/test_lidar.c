#include <lib/sensor/lidar.h>

#include "unity_fixture.h"

extern circular_buffer_t cbuf;
extern uart_t uart_mock;

extern driver_pwm_t pwm_mock;

float mock_setted_duty, mock_delayed_ms;

static void mock_delay_ms(float ms) {
    mock_delayed_ms = ms;
}

retval_t mock_tfminis_update(tfminis_t *tfmini) {
    static float times_called = 0;
    tfmini->data.dist_cm = times_called;
    times_called++;
}

tfminis_t tfmini_mock = {
    .data = _DECLARE_TFMINIS_DEFAULT_DATA(),
    .uart = &uart_mock,
    .update = mock_tfminis_update,
    .init = tfminis_init,
};

static DEFINE_SERVO(mock_servo, &pwm_mock, &mock_delay_ms);
static lidar_sensor_config_t lidar_cfg;
static DEFINE_LIDAR(lidar, &lidar_cfg, &tfmini_mock, &mock_servo);

TEST_GROUP(lidar);

TEST_SETUP(lidar) {}
TEST_TEAR_DOWN(lidar) {}

TEST(lidar, test_regular_operation) {
    lidar.init(&lidar);
    lidar.scan(&lidar);
    for (size_t i = 0; i < LIDAR_MAX_SAMPLES; i++) {
        TEST_ASSERT_EQUAL_FLOAT(i, lidar.data[i].dist_cm);
    }
}

TEST_GROUP_RUNNER(lidar) {
    RUN_TEST_CASE(lidar, test_regular_operation);
}

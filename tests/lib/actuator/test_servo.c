#include <lib/common/circular_buffer.h>
#include <lib/driver/pwm.h>
#include <lib/actuator/servo.h>

#include "unity_fixture.h"

float mock_setted_duty, mock_delayed_ms;

static void mock_delay_ms(float ms) {
    mock_delayed_ms = ms;
}

extern float mock_delayed_ms, mock_setted_duty;
extern driver_pwm_t pwm_mock;

static DEFINE_SERVO(test_servo, &pwm_mock, &mock_delay_ms);

TEST_GROUP(servo);

TEST_SETUP(servo) {}
TEST_TEAR_DOWN(servo) {}

TEST(servo, test_regular_operation) {
    retval_t rv;
    rv = test_servo.init(&test_servo);
    TEST_ASSERT_EQUAL(RV_SUCCESS, rv);


    rv = actuator_servo_set_pos_blocking(&test_servo, 0);
    TEST_ASSERT_EQUAL_FLOAT(7.5, mock_setted_duty);

    rv = actuator_servo_set_pos_blocking(&test_servo, 90);
    TEST_ASSERT_EQUAL_FLOAT(10.0, mock_setted_duty);
    TEST_ASSERT_EQUAL_FLOAT(170.0, mock_delayed_ms);

    rv = actuator_servo_set_pos_blocking(&test_servo, -90);
    TEST_ASSERT_EQUAL_FLOAT(5.0, mock_setted_duty);
    TEST_ASSERT_EQUAL_FLOAT(320.0, mock_delayed_ms);

    rv = actuator_servo_set_pos_blocking(&test_servo, -89);
    TEST_ASSERT_EQUAL_FLOAT(5.027778, mock_setted_duty);
    TEST_ASSERT_EQUAL_FLOAT(21.66667, mock_delayed_ms);

    float to_delay;
    rv = actuator_servo_set_pos(&test_servo, -88, &to_delay);
    TEST_ASSERT_EQUAL_FLOAT(5.055556, mock_setted_duty);
    TEST_ASSERT_EQUAL_FLOAT(21.66667, to_delay);
}

TEST_GROUP_RUNNER(servo) {
    RUN_TEST_CASE(servo, test_regular_operation);
}

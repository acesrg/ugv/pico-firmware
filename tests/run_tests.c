#include "unity_fixture.h"

int main(void)
{
    UNITY_BEGIN();
    RUN_TEST_GROUP(dummy);
    RUN_TEST_GROUP(circular_buffer);
    RUN_TEST_GROUP(tfminis);
    RUN_TEST_GROUP(servo);
    RUN_TEST_GROUP(lidar);
    return UNITY_END();
}

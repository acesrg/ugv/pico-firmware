/* Copyright 2023 ACES-RG */
#include <hardware/gpio.h>

#include <lib/driver/gpio.h>


retval_t gpio_driver_init(gpio_t *gpio) {
    gpio_state_t state;
    gpio_init(gpio->config->gpio);
    gpio_set_dir(gpio->config->gpio, gpio->config->dir);

    if (GPIO_DIR_OUTPUT == gpio->config->dir && GPIO_STATE_UNDEFINED != gpio->config->init_state) {
        gpio_put(gpio->config->gpio, gpio->config->init_state);
    }

    state = gpio->get(gpio);
    gpio->_state_cached = state;
}

retval_t gpio_driver_on(gpio_t *gpio) {
    gpio_put(gpio->config->gpio, GPIO_STATE_ON);
    gpio->_state_cached = GPIO_STATE_ON;
}

retval_t gpio_driver_off(gpio_t *gpio) {
    gpio_put(gpio->config->gpio, GPIO_STATE_OFF);
    gpio->_state_cached = GPIO_STATE_OFF;
}

retval_t gpio_driver_toggle(gpio_t *gpio) {
    switch (gpio->get(gpio)) {
        case GPIO_STATE_ON:
            gpio->off(gpio);
            break;

        case GPIO_STATE_OFF:
            gpio->on(gpio);
            break;

        default:
            break;
    }
}

gpio_state_t gpio_driver_get(gpio_t *gpio) {
    if (gpio_get(gpio->config->gpio)) {
        return GPIO_STATE_ON;
    }
    return GPIO_STATE_OFF;
}

/* Copyright 2023 ACES-RG */
/*3rd-party libs*/
#include <pico/stdlib.h>
#include <hardware/pwm.h>
/* local libs */
#include <lib/driver/pwm.h>

/*
 * \brief   The pwm frequency is defined by:
 *
 *          CPU_CLK_FREQUENCY / (DRIVER_PWM_TOP * div)
 *
 *          The minimum and maximum frequency is limited
 *          with the prescaler, which is of the float
 *          type from 1.f to 255.f.
 *          The maximum count TOP of the PWM counter
 *          is set to 2^15 (32768). This defines the
 *          minimum and maximum frequency that can be
 *          obtained defined in the macros.
 */

/* macros */
#define CPU_CLK_FREQUENCY                   125000000
#define DRIVER_PWM_TOP                      32768
#define DRIVER_PWM_PROPORTION_FREQ          ((float)(CPU_CLK_FREQUENCY) / (float)(DRIVER_PWM_TOP))
#define DRIVER_PWM_PROPORTION_DUTY          ((float)(DRIVER_PWM_TOP) / 100.f)
#define DRIVER_PWM_FREQUENCY_MIN            15
#define DRIVER_PWM_FREQUENCY_MAX            3814
#define DRIVER_PWM_DIV_MIN                  1.f
#define DRIVER_PWM_DIV_MAX                  255.f
#define DRIVER_PWM_INIT_COUNT               0


static float prescaler_calcule(uint16_t freq) {
    if (freq < DRIVER_PWM_FREQUENCY_MIN) return DRIVER_PWM_DIV_MAX;
    if (freq > DRIVER_PWM_FREQUENCY_MAX) return DRIVER_PWM_DIV_MIN;
    return (DRIVER_PWM_PROPORTION_FREQ / (float)(freq));
}

retval_t driver_pwm_init(driver_pwm_t *driver) {
    /* Get a set of default values for PWM configuration. */
    pwm_config cfg = pwm_get_default_config();
    /* Determine the PWM slice and channel that is attached to the specified GPIO. */
    uint slice_num = pwm_gpio_to_slice_num(driver->config.driver_pwm_out);
    uint channel = pwm_gpio_to_channel(driver->config.driver_pwm_channel);
    /* Set phase correction in a PWM configuration. */
    pwm_config_set_phase_correct(&cfg, true);
    /* Sets the clock division and maximum count. */
    pwm_set_clkdiv(slice_num, prescaler_calcule(driver->config.driver_pwm_frequency));
    pwm_config_set_clkdiv_mode(&cfg, PWM_DIV_FREE_RUNNING);
    pwm_set_counter(slice_num, DRIVER_PWM_INIT_COUNT);
    pwm_set_wrap(slice_num, DRIVER_PWM_TOP);
    /* Enable PWM output. */
    gpio_set_function(driver->config.driver_pwm_out, GPIO_FUNC_PWM);
    pwm_set_enabled(slice_num, true);
    return RV_SUCCESS;
}

retval_t driver_pwm_set_duty(driver_pwm_t *driver, float duty) {
    uint slice_num = pwm_gpio_to_slice_num(driver->config.driver_pwm_out);
    pwm_set_chan_level(slice_num, driver->config.driver_pwm_channel, (duty * DRIVER_PWM_PROPORTION_DUTY));
    return RV_SUCCESS;
}

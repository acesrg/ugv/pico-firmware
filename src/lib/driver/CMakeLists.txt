add_library(aces_uart
    src/uart.c
)

target_link_libraries(aces_uart
    pico_stdlib          # core functionality
    hardware_uart        # uart
    aces_circular_buffer
)

target_include_directories(
    aces_uart
    PUBLIC
    ../..
)

add_library(aces_pwm
    src/pwm.c
)

target_link_libraries(aces_pwm
    pico_stdlib          # core functionality
    hardware_pwm         # pwm
)

target_include_directories(
    aces_pwm
    PUBLIC
    ../..
)

add_library(aces_gpio
    src/gpio.c
)

target_link_libraries(aces_gpio
    hardware_gpio        # pwm
)

target_include_directories(
    aces_gpio
    PUBLIC
    ../..
)

/* Copyright 2023 ACES-RG */
#include <lib/driver/gpio.h>
#include <lib/driver/pwm.h>
#include <lib/actuator/motor.h>

#define MOTOR_CW_1              1
#define MOTOR_CCW_1             2
#define DRIVER_PWM_OUT          0
#define DRIVER_PWM_CHANNEL      0
#define DRIVER_PWM_FREQUENCY    1000

static gpio_config_t power_gpio_cfg = {
    .gpio = 0,
    .dir = GPIO_DIR_OUTPUT,
    .init_state = GPIO_STATE_ON,
};
static DEFINE_GPIO(power_gpio, &power_gpio_cfg);

static gpio_config_t cw_gpio_cfg = {
    .gpio = 1,
    .dir = GPIO_DIR_OUTPUT,
    .init_state = GPIO_STATE_OFF,
};
static DEFINE_GPIO(cw_gpio, &cw_gpio_cfg);

static gpio_config_t ccw_gpio_cfg = {
    .gpio = 2,
    .dir = GPIO_DIR_OUTPUT,
    .init_state = GPIO_STATE_ON,
};
static DEFINE_GPIO(ccw_gpio, &ccw_gpio_cfg);


static DEFINE_PWM(motor_pwm, DRIVER_PWM_OUT, DRIVER_PWM_CHANNEL, DRIVER_PWM_FREQUENCY);
static DEFINE_MOTOR(test_motor, &motor_pwm, &power_gpio, &cw_gpio, &ccw_gpio);

/*
static driver_pwm_config_t driver_pwm_config = {
    .driver_pwm_out = DRIVER_PWM_OUT,
    .driver_pwm_channel = DRIVER_PWM_CHANNEL,
    .driver_pwm_frequency = DRIVER_PWM_FREQUENCY,
};
*/

retval_t motor_power(motor_t *motor) {
    motor->cw_gpio->init(motor->cw_gpio);
    motor->ccw_gpio->init(motor->ccw_gpio);
    motor->power_gpio->init(motor->power_gpio);
    // driver_pwm_init(&driver_pwm_config);
    // driver_pwm_set_duty(&driver_pwm_config, 50.0);
    return RV_SUCCESS;
}

/* Copyright 2023 ACES-RG */
#include <stddef.h>

#include <lib/actuator/servo.h>


retval_t actuator_servo_init(servo_t *servo) {
    SUCCESS_OR_RETURN(servo->pwm->init(servo->pwm));
    SUCCESS_OR_RETURN(servo->set_pos(servo, NG90_TOP_DEG_NEG, NULL));

    /* cast given pointer to a delay function */
    typedef void (*delay_ms_f)(float ms);
    delay_ms_f delay_ms;
    delay_ms = (delay_ms_f)servo->delay_ms_fn;
    delay_ms(NG90_INIT_TIME_MS);
    return RV_SUCCESS;
}

static inline float ng90_deg_to_duty(float deg) {
    return ((deg / NG90_SPAN) + NG90_PWM_ACTIVE_MS_0) * 5.f;
}

static inline float ng90_wait_time(float deg) {
    float time = deg / NG90_SPEED_DEG_MS;
    time = time > 0 ? time : -time;
    return time + NG90_PWM_PERIOD_MS;
}

retval_t actuator_servo_set_pos(servo_t *servo, float pos, float *delay_ms) {
    SUCCESS_OR_RETURN(servo->pwm->set_duty(servo->pwm, ng90_deg_to_duty(pos)));
    if (delay_ms != NULL) {
        *delay_ms = ng90_wait_time(servo->_pos_deg - pos);
    }
    servo->_pos_deg = pos;
    return RV_SUCCESS;
}

retval_t actuator_servo_set_pos_blocking(servo_t *servo, float pos) {
    if (NULL == servo->delay_ms_fn) {
        return RV_ILLEGAL;
    }
    SUCCESS_OR_RETURN(servo->pwm->set_duty(servo->pwm, ng90_deg_to_duty(pos)));

    /* cast given pointer to a delay function */
    typedef void (*delay_ms_f)(float ms);
    delay_ms_f delay_ms;
    delay_ms = (delay_ms_f)servo->delay_ms_fn;
    delay_ms(ng90_wait_time(servo->_pos_deg - pos));
    servo->_pos_deg = pos;
    return RV_SUCCESS;
}

/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_ACTUATOR_MOTOR_H_
#define SRC_LIB_ACTUATOR_MOTOR_H_
/* local libs */
#include <lib/common/retval.h>

typedef struct motor_s motor_t;

typedef retval_t (motor_power_f)(motor_t *motor);

struct motor_s {
    driver_pwm_t *pwm;
    gpio_t *power_gpio;
    gpio_t *cw_gpio;
    gpio_t *ccw_gpio;
    motor_power_f *power;
};

// retval_t motor_power();
retval_t motor_power(motor_t *motor);
// retval_t motor_break();
// retval_t motor_set_direction();
// retval_t motor_set_duty();

#define DEFINE_MOTOR(_name, _pwm, _power_gpio, _cw_gpio, _ccw_gpio) \
    motor_t _name = {                                               \
        .pwm = _pwm,                                                \
        .power_gpio = _power_gpio,                                  \
        .cw_gpio = _cw_gpio,                                        \
        .ccw_gpio = _ccw_gpio,                                      \
        .power = motor_power,                                       \
    }
#endif  // SRC_LIB_ACTUATOR_MOTOR_H_

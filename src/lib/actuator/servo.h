/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_ACTUATOR_SERVO_H_
#define SRC_LIB_ACTUATOR_SERVO_H_
#include <lib/common/retval.h>
#include <lib/driver/pwm.h>

/*
 *    _
 *   | |
 *   | |__________________
 *   <-- 20 ms (50 Hz) -->
 *   <-> 1-2 ms active
 *
 * Active cycle/deg ratio:
 *  1 ms    (5%)    -> -90°
 *  1.5 ms  (7.5%)  -> 0°
 *  2 ms    (10%)   -> 90°
 */
#define NG90_SPEED_DEG_MS           0.6f  // 0.10s to do 60°
#define NG90_PWM_FREQ_HZ            50.0f
#define NG90_PWM_PERIOD_MS          ((1.0f / NG90_PWM_FREQ_HZ) * 1000.0f)
#define NG90_PWM_ACTIVE_MS_0        1.5f
#define NG90_TOP_DEG_NEG            -90.0f
#define NG90_TOP_DEG_POS            90.0f
#define NG90_SPAN                   (NG90_TOP_DEG_POS - NG90_TOP_DEG_NEG)
#define NG90_DEG_PER_MS             NG90_SPAN / 1.0f
#define NG90_DEADBAND_US            10.0f
#define NG90_STEP_DEG               ((NG90_DEADBAND_US / 1000.0f) * (NG90_DEG_PER_MS))
#define NG90_INIT_TIME_MS           270.f

typedef struct servo_s servo_t;

typedef retval_t (servo_init_f)(servo_t *servo);
typedef retval_t (servo_set_f)(servo_t *servo, float pos, float *delay_ms);
typedef retval_t (servo_set_block_f)(servo_t *servo, float pos);

struct servo_s {
    driver_pwm_t *pwm;
    float _pos_deg;
    servo_init_f *init;
    servo_set_f *set_pos;
    servo_set_block_f *set_pos_blocking;
    void *delay_ms_fn;
};

retval_t actuator_servo_init(servo_t *servo);

/*
 * set position and return inmediatly
 * "delay_ms" pointer returns how much will
 * it take for the servo to reach "pos", if
 * NULL is passed on delay_ms this feature
 * is ignored
 */
retval_t actuator_servo_set_pos(servo_t *servo, float pos, float *delay_ms);

/*
 * set position, returns only when servo
 * had enough time to reach "pos"
 */
retval_t actuator_servo_set_pos_blocking(servo_t *servo, float pos);

#define DEFINE_SERVO(_name, _pwm, _delay_ms_fn)                 \
    servo_t _name = {                                           \
        .pwm = _pwm,                                            \
        ._pos_deg = 0,                                          \
        .init = actuator_servo_init,                            \
        .set_pos = actuator_servo_set_pos,                      \
        .set_pos_blocking = actuator_servo_set_pos_blocking,    \
        .delay_ms_fn = _delay_ms_fn,                            \
    }
#endif  // SRC_LIB_ACTUATOR_SERVO_H_

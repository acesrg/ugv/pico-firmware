/* Copyright 2023 ACES-RG */
#ifndef SRC_LIB_SENSOR_LIDAR_H_
#define SRC_LIB_SENSOR_LIDAR_H_
#include <stddef.h>

#include <lib/actuator/servo.h>
#include <lib/common/retval.h>
#include <lib/sensor/tfminis.h>
#define LIDAR_MAX_SAMPLES       100  // (size_t)(NG90_SPAN / NG90_STEP_DEG)

typedef struct lidar_sensor_s lidar_sensor_t;

typedef retval_t (lidar_init_f)(lidar_sensor_t *lidar);
typedef retval_t (lidar_scan_f)(lidar_sensor_t *lidar);

typedef struct {
    float dist_cm;
    float strenght;
    float angle;
} lidar_sensor_datapoint_t;

typedef struct {
    float beam_width_deg;
    float beam_resolution_deg;
    float point_delay_ms;
} lidar_sensor_config_t;  // TODO(marcotti): borrar

struct lidar_sensor_s {
    lidar_sensor_config_t *config;
    lidar_sensor_datapoint_t data[LIDAR_MAX_SAMPLES];
    size_t data_size;
    tfminis_t *sensor;
    servo_t *servo;
    lidar_init_f *init;
    lidar_scan_f *scan;
    lidar_scan_f *scan_point;
};

retval_t lidar_sensor_init(lidar_sensor_t *lidar);
retval_t lidar_sensor_scan(lidar_sensor_t *lidar);
retval_t lidar_sensor_point_scan(lidar_sensor_t *lidar);

#define DEFINE_LIDAR(_name, _config, _sensor, _servo)   \
    lidar_sensor_t _name = {                            \
        .config = _config,                              \
        .data = {},                                     \
        .data_size = LIDAR_MAX_SAMPLES,                 \
        .sensor = _sensor,                              \
        .servo = _servo,                                \
        .init = lidar_sensor_init,                      \
        .scan = lidar_sensor_scan,                      \
    }

#endif  // SRC_LIB_SENSOR_LIDAR_H_

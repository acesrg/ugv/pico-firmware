/* Copyright 2023 ACES-RG */
#include <lib/sensor/lidar.h>

retval_t lidar_sensor_init(lidar_sensor_t *lidar) {
    SUCCESS_OR_RETURN(lidar->servo->init(lidar->servo));
    SUCCESS_OR_RETURN(lidar->sensor->init(lidar->sensor));
    return RV_SUCCESS;
}

retval_t lidar_sensor_scan(lidar_sensor_t *lidar) {
    for (size_t i = 0; i < LIDAR_MAX_SAMPLES; i++) {
        float angle = NG90_TOP_DEG_NEG + i * NG90_STEP_DEG;
        lidar->sensor->update(lidar->sensor);

        lidar->data[i].dist_cm = lidar->sensor->data.dist_cm;
        lidar->data[i].strenght = lidar->sensor->data.strenght;
        lidar->data[i].angle = angle;

        lidar->servo->set_pos_blocking(lidar->servo, angle);
    }
    return RV_SUCCESS;
}

retval_t lidar_sensor_point_scan(lidar_sensor_t *lidar) {
    return RV_SUCCESS;
}

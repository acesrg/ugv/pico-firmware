/* Copyright 2023 ACES-RG */
#include <lib/common/retval.h>
#include <lib/sensor/tfminis.h>
#define TFMINI_UART_HEADER 0x59
#define TFMINI_TEMPERATURE(x) ((((float)x)/8.0f) - 256.0f)

/*
 * |  0  |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |
 * |  HEADER   |DIST |DIST |STREN|STREN|TEMP |TEMP |CHECK|
 * |0x59 | 0x59|LOW  |HIGH |LOW  |HIGH |LOW  |HIGH |     |
 **/
typedef struct {
    uint8_t dist_low;
    uint8_t dist_high;
    uint8_t strenght_low;
    uint8_t strenght_high;
    uint8_t temp_low;
    uint8_t temp_high;
    uint8_t checksum;
} tfminis_uart_packet_t;

static retval_t calculate_tfminis_checksum(tfminis_uart_packet_t *pkt) {
    uint16_t sum = TFMINI_UART_HEADER + TFMINI_UART_HEADER + \
                   pkt->dist_high + pkt->dist_low + \
                   pkt->strenght_high + pkt->strenght_low + \
                   pkt->temp_high + pkt->temp_low;
    if ((uint8_t) sum != pkt->checksum) {
        return RV_ERROR;
    }
    return RV_SUCCESS;
}

static retval_t tfmini_read_until_header(tfminis_t *tfmini) {
    frame_t frame = DECLARE_FRAME_SPACE(1);
    retval_t rv;
    while (true) {
        do {
            rv = circular_buffer_read_frame(tfmini->uart->cbuf, &frame);
        } while (rv != RV_SUCCESS);
        if (frame.buf[0] == TFMINI_UART_HEADER) {
            do {
                rv = circular_buffer_read_frame(tfmini->uart->cbuf, &frame);
            } while (rv != RV_SUCCESS);
            if (frame.buf[0] == TFMINI_UART_HEADER) {
                return RV_SUCCESS;
            }
        }
    }
}

static retval_t decode_serial_packets(tfminis_t *tfmini) {
    tfmini_read_until_header(tfmini);

    frame_t frame = DECLARE_FRAME_SPACE(7);
    retval_t rv;
    do {
        rv = circular_buffer_read_frame(tfmini->uart->cbuf, &frame);
    } while (rv != RV_SUCCESS);

    tfminis_uart_packet_t *pkt = (tfminis_uart_packet_t*) frame.buf;

    SUCCESS_OR_RETURN(calculate_tfminis_checksum(pkt));

    tfmini->data.dist_cm = (pkt->dist_high << 8) + pkt->dist_low;
    tfmini->data.strenght = (pkt->strenght_high << 8) + pkt->strenght_low;
    tfmini->data.temp_c = TFMINI_TEMPERATURE((pkt->temp_high << 8) + pkt->temp_low);
    return RV_SUCCESS;
}

retval_t tfminis_update(tfminis_t *tfmini) {
    return decode_serial_packets(tfmini);
}

retval_t tfminis_init(tfminis_t *tfmini) {
    return tfmini->uart->init(tfmini->uart);
}

/* Copyright 2023 ACES-RG */
#include <lib/common/circular_buffer.h>


static size_t _get_readable_bytes(circular_buffer_t *cbuf) {
    if (cbuf->full) {
        return cbuf->size;
    }
    int diff = cbuf->write_pos - cbuf->read_pos;
    size_t available = diff < 0 ? diff + cbuf->size : diff;
    return available;
}

static size_t _get_writable_bytes(circular_buffer_t *cbuf) {
    if (cbuf->full) {
        return 0;
    }
    int diff = cbuf->read_pos - cbuf->write_pos;
    size_t available = diff <= 0 ? diff + cbuf->size: diff;
    return available;
}

static void _increment_write_pos(circular_buffer_t *cbuf) {
    cbuf->write_pos++;
    if (cbuf->size == cbuf->write_pos) {
        cbuf->write_pos = 0;
    }
    if (cbuf->write_pos == cbuf->read_pos) {
        cbuf->full = true;
    }
}

static void _increment_read_pos(circular_buffer_t *cbuf) {
    cbuf->read_pos++;
    if (cbuf->size == cbuf->read_pos) {
        cbuf->read_pos = 0;
    }
    cbuf->full = false;
}

retval_t circular_buffer_read_frame(circular_buffer_t *cbuf, frame_t *frame) {
    if (NULL == cbuf || NULL == frame || frame->size > cbuf->size) {
        return RV_ILLEGAL;
    }
    if (frame->size > _get_readable_bytes(cbuf)) {
        return RV_NOSPACE;
    }
    for (frame->pos = 0; frame->pos < frame->size; frame->pos++) {
        frame->buf[frame->pos] = cbuf->buf[cbuf->read_pos];
        _increment_read_pos(cbuf);
    }
    return RV_SUCCESS;
}

retval_t circular_buffer_write_frame(circular_buffer_t *cbuf, frame_t *frame) {
    if (NULL == cbuf || NULL == frame || frame->size > cbuf->size) {
        return RV_ILLEGAL;
    }
    if (frame->size > _get_writable_bytes(cbuf)) {
        return RV_NOSPACE;
    }
    for (frame->pos = 0; frame->pos < frame->size; frame->pos++) {
        cbuf->buf[cbuf->write_pos] = frame->buf[frame->pos];
        _increment_write_pos(cbuf);
    }
    return RV_SUCCESS;
}

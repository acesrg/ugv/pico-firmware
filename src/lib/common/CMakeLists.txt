add_library(aces_circular_buffer
    src/circular_buffer.c
)

include_directories(
    PUBLIC
    ../..
)

target_include_directories(
    aces_circular_buffer
    PUBLIC
    ../..
)


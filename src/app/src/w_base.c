/* Copyright 2023 ACES-RG */
#include <stdio.h>

#include <pico/stdlib.h>

#include <FreeRTOS.h>
#include <task.h>

#include <lib/sensor/lidar.h>

#define MOTOR_PERIOD_MS         1000
#define MOTOR_PERIOD            pdMS_TO_TICKS(MOTOR_PERIOD_MS)


static DEFINE_CIRCULAR_BUFFER(cbuf, 9);
static DEFINE_UART(uart_tfmini, 1, 115200, 8, 1, UART_PARITY_NONE, 8, 9, &cbuf);
static DEFINE_TFMINIS(tfmini, &uart_tfmini);

#define SERVO_PWM_GPIO_PIN  20
#define SERVO_PWM_CHANNEL   0
#define SERVO_PWM_FREQUENCY 50
static DEFINE_PWM(servo_pwm, SERVO_PWM_GPIO_PIN, SERVO_PWM_CHANNEL, SERVO_PWM_FREQUENCY);

static void delay_ms(float ms) {
    vTaskDelay((TickType_t)(ms / portTICK_PERIOD_MS));
}
static DEFINE_SERVO(lidar_servo, &servo_pwm, delay_ms);
static lidar_sensor_config_t lidar_cfg;
static DEFINE_LIDAR(lidar, &lidar_cfg, &tfmini, &lidar_servo);


void tfmini_update(void *pvParameters) {
    stdio_init_all();
    lidar.init(&lidar);
    vTaskDelay(10 / portTICK_PERIOD_MS);
    for (;;) {
        lidar.scan(&lidar);
        printf("angle = [ ");
        for (size_t i = 0; i < lidar.data_size; i ++) {
            printf("%f,", lidar.data[i].angle);
        }
        printf("]\n");
        printf("dist = [ ");
        for (size_t i = 0; i < lidar.data_size; i ++) {
            printf("%f,", lidar.data[i].dist_cm);
        }
        printf("]\n");
        printf("strenght = [ ");
        for (size_t i = 0; i < lidar.data_size; i ++) {
            printf("%f,", lidar.data[i].strenght);
        }
        printf("]\n");
        lidar.init(&lidar);
        vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

int main() {
    xTaskCreate(&tfmini_update, "TFMINIS update", 256, NULL, 2, NULL);
    vTaskStartScheduler();
    while (1) {
        // hi
    }
}
